//
//  Record+CoreDataProperties.swift
//  TodoList
//
//  Created by Vicki Yang on 2020/4/21.
//  Copyright © 2020 Vicki Yang. All rights reserved.
//
//

import Foundation
import CoreData


extension Record {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Record> {
        return NSFetchRequest<Record>(entityName: "Record")
    }

    @NSManaged public var seq: NSNumber?
    @NSManaged public var id: NSNumber?
    @NSManaged public var done: NSNumber?
    @NSManaged public var content: String?

}
