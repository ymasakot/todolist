//
//  CheckedRecordsViewController.swift
//  TodoList
//
//  Created by Vicki Yang on 2020/4/22.
//  Copyright © 2020 Vicki Yang. All rights reserved.
//

import UIKit

class CheckedRecordsViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        self.title = "已完成事項"
        self.automaticallyAdjustsScrollViewInsets = false
        checkBoxStatus = true
        
        //UITableView
        myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: fullsize.width, height: fullsize.height - 60), style: .plain)
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.allowsSelection = true
        self.view.addSubview(myTableView)
    }
}
