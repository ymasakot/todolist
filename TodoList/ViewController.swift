//
//  ViewController.swift
//  TodoList
//
//  Created by Vicki Yang on 2020/4/21.
//  Copyright © 2020 Vicki Yang. All rights reserved.
//

import UIKit

class ViewController: BaseViewController {
    var myTextField : UITextField!
    var addBtn : UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "TodoList"
        checkBoxStatus = false
        
        //more btn
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(image: UIImage(named: "more"), style: .plain, target: self, action: #selector(moreBtnAction))
        
        //UITableView
        myTableView = UITableView(frame: CGRect(x: 0, y: 45, width: fullsize.width, height: fullsize.height - 100), style: .plain)
        myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        myTableView.delegate = self
        myTableView.dataSource = self
        myTableView.allowsSelection = true
        self.view.addSubview(myTableView)
        
        //textField
        myTextField = UITextField(frame: CGRect(x: 10, y: 5, width: fullsize.width - 55 , height: 35))
        myTextField.backgroundColor = #colorLiteral(red: 0.9347512123, green: 0.9347512123, blue: 0.9347512123, alpha: 1)
        myTextField.delegate = self
        myTextField.placeholder = "新增事項"
        myTextField.leftView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 35))
        myTextField.leftViewMode = .always
        myTextField.returnKeyType = .done
        self.view.addSubview(myTextField)
        
        //addBtn
        addBtn = UIButton(type: .contactAdd)
        addBtn.center = CGPoint(x: fullsize.width - 22, y: 22)
        addBtn.addTarget(self, action: #selector(addBtnAction), for: .touchUpInside)
        self.view.addSubview(addBtn)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        myTableView.setEditing(true, animated: false)
        self.editBtnAction()
    }
    
    // MARK: Btn actions
    @objc func moreBtnAction() {
        self.navigationController?.pushViewController(MoreViewController(), animated: true)
    }
    
    @objc func addBtnAction() {
        //隱藏鍵盤
        self.view.endEditing(true)
        
        let content = myTextField.text ?? ""
        
        if content != "" {
            //get max seq
            var seq = 100
            let getResults = coreDataConnect.retrieve(entityName, predicate: "done = false", sort: [["id" : true]], limit: 1)
            if let results = getResults {
                for result in results {
                    seq = (result.value(forKey: "seq") as! Int) + 1
                }
            }
            
            var id = 1
            if let idSeq = myUserDafaults.object(forKey: "idSeq") as? Int {
                id = idSeq + 1
            }
            
            let insertResult = coreDataConnect.insert(entityName, attributeInfo: [
                "id" : "\(id)",
                "content" : content,
                "seq" : "\(seq)",
                "done" : "false"
            ])
            
            if insertResult {
                print("add data success")
                
                myTextField.text = ""
                
                //get new record
                let newRecord = coreDataConnect.retrieve(entityName, predicate: "id = \(id)", sort: nil, limit: 1)
                myRecords.insert(newRecord![0] as! Record, at: 0)
                
                //update tableView
                myTableView.beginUpdates()
                myTableView.insertRows(at: [IndexPath(row: 0, section: 0)], with: .fade)
                myTableView.endUpdates()
                
                //update auto increment
                myUserDafaults.set(id, forKey: "idSeq")
                myUserDafaults.synchronize()
            }
        }
    }
    
    @objc func editBtnAction() {
        myTableView.setEditing(!myTableView.isEditing, animated: true)
        if (!myTableView.isEditing) {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "編輯", style: .plain, target: self, action: #selector(editBtnAction))
            
            //可新增待辦事項
            myTextField.isUserInteractionEnabled = true
            addBtn.isEnabled = true
            
            //show checkbox btn
            for record in myRecords {
                if let id = record.id {
                    let btn = self.view.viewWithTag(checkBoxTag + id.intValue) as? UIButton
                    btn?.isHidden = false
                }
            }
        } else {
            self.navigationItem.leftBarButtonItem = UIBarButtonItem(title: "完成", style: .plain, target: self, action: #selector(editBtnAction))
            
            //隱藏鍵盤
            self.view.endEditing(true)
            
            //無法新增待辦事項
            myTextField.isUserInteractionEnabled = false
            addBtn.isEnabled = false
            
            //hide checkbox btn
            for record in myRecords {
                if let id = record.id {
                    let btn = self.view.viewWithTag(checkBoxTag + id.intValue) as? UIButton
                    btn?.isHidden = true
                }
            }
        }
    }
    
    // MARK: UITextFieldDelegate delegate methods
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.addBtnAction()
        return true
    }
    
    // MARK: UITableView Delegate methods
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        super.tableView(tableView, didSelectRowAt: indexPath)
        self.updateRecordContent(indexPath)
    }

    //編輯狀態時 拖曳切換 cell 位置後執行動作的方法 (必須實作這個方法才會出現排序功能)
    func tableView(_ tableView: UITableView, moveRowAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        print("\(sourceIndexPath.row) to \(destinationIndexPath.row)")
        
        var tempArr:[Record] = []
        
        if (sourceIndexPath.row > destinationIndexPath.row) { //排在下的往上移動
            for (index, value) in myRecords.enumerated() {
                if index < destinationIndexPath.row || index > sourceIndexPath.row { //異動範圍外
                    tempArr.append(value)
                } else if index == destinationIndexPath.row {
                    tempArr.append(myRecords[sourceIndexPath.row])
                } else if index <= sourceIndexPath.row {
                    tempArr.append(myRecords[index - 1])
                }
            }
        } else if (sourceIndexPath.row < destinationIndexPath.row) { //排在上的往下移動
            for (index, value) in myRecords.enumerated() {
                if index < sourceIndexPath.row || index > destinationIndexPath.row { //異動範圍外
                    tempArr.append(value)
                } else if index < destinationIndexPath.row {
                    tempArr.append(myRecords[index + 1])
                } else if index == destinationIndexPath.row {
                    tempArr.append(myRecords[sourceIndexPath.row])
                }
            }
        } else {
            tempArr = myRecords
        }
        myRecords = tempArr
        self.updateRecordsSeq()
    }

    //update 事項內容
    func updateRecordContent(_ indexPath: IndexPath) {
        let content = myRecords[indexPath.row].content!
        let id = myRecords[indexPath.row].id!.intValue
        
        let updateAlertController = UIAlertController(title: "更新", message: nil, preferredStyle: .alert)
        updateAlertController.addTextField {
            (textField: UITextField!) -> Void in
            textField.text = content
            textField.placeholder = "更新事項"
        }
        
        //cancel btn
        let cancelAction = UIAlertAction(title: "取消", style: .cancel, handler: nil)
        updateAlertController.addAction(cancelAction)
        
        //ok btn
        let okAction = UIAlertAction(title: "確認", style: UIAlertAction.Style.default) {
            (action: UIAlertAction) -> Void in
            let updateContent = (updateAlertController.textFields?.first)! as UITextField
            
            let result = self.coreDataConnect.update(self.entityName, predicate: "id = \(id)", attributeInfo: ["content" : "\(updateContent.text!)"])
            if result {
                let newRecord = self.coreDataConnect.retrieve(self.entityName, predicate: "id = \(id)", sort: nil, limit: 1)
                
                self.myRecords[indexPath.row] = newRecord![0] as! Record
                self.myTableView.reloadData()
                print("update data success")
            } else {
                print("update data fail")
            }
        }
        updateAlertController.addAction(okAction)
        
        self.present(updateAlertController, animated: true, completion: nil)
    }
    
    //update Core data 中的事項資料排序
    func updateRecordsSeq() {
        var count = myRecords.count
        
        for record in myRecords {
            let result = coreDataConnect.update(self.entityName, predicate: "id = \(record.id!)", attributeInfo: ["seq" : "\(count)"])
            if result {
                print("update seq \(record.id!) : \(count)")
            } else {
                print("update seq error")
            }
            
            count -= 1
        }
    }
}

