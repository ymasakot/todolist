//
//  BaseViewController.swift
//  TodoList
//
//  Created by Vicki Yang on 2020/4/21.
//  Copyright © 2020 Vicki Yang. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate {
    let fullsize = UIScreen.main.bounds.size
    let myUserDafaults = UserDefaults.standard
    let managedObjectContext = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    let entityName = "Record"
    var coreDataConnect : CoreDataConnect!
    var myRecords : [Record]! = []
    var myTableView : UITableView!
    var checkBoxStatus = false
    var checkBoxTag = 1000
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        coreDataConnect = CoreDataConnect(context: self.managedObjectContext) //connect Core Data
        
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        let getResults = coreDataConnect.retrieve(entityName, predicate: "done = \(checkBoxStatus ? "true" : "false")"
            , sort: [["seq" : false], ["id" : false]], limit: nil)
        if let results = getResults {
            myRecords = results as? [Record]
        }
        
        myTableView.reloadData()
    }
    
    //MARK : UITableView Delegate methods
    
    //每組有幾個 cell
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return myRecords.count
    }

    //每個 cell 顯示的內容
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //取得 tableView 目前使用的 cell
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        
        cell.textLabel?.text = "\(myRecords[indexPath.row].content ?? "")"
        
        
        //移除舊的按鈕
        for view in cell.contentView.subviews {
            if let v = view as? UIButton {
                v.removeFromSuperview()
            }
        }

        //點選完成事項
        let checkBoxBtn = UIButton(frame: CGRect(x: Double(fullsize.width) - 42, y: 2, width: 40, height: 40))
        checkBoxBtn.tag = checkBoxTag + (myRecords[indexPath.row].id ?? 0).intValue
        checkBoxBtn.setImage(UIImage(named:(checkBoxStatus ? "check" : "checkbox")), for: .normal)
        checkBoxBtn.addTarget(self, action: #selector(checkBtnAction), for: .touchUpInside)
        cell.contentView.addSubview(checkBoxBtn)
        
        return cell
    }
    
    //點選 cell 後執行的動作
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.view.endEditing(true)
        
        // 取消 cell 的選取狀態
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    //各 cell 是否可以進入編輯狀態 及 左滑刪除
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    //編輯狀態時 按下刪除 cell 後執行動作的方法 (另外必須實作這個方法才會出現左滑刪除功能)
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        let id = myRecords[indexPath.row].id
        
        if editingStyle == .delete {
            if coreDataConnect.delete(entityName, predicate: "id = \(id!)") {
                myRecords.remove(at: indexPath.row)
                
                tableView.beginUpdates()
                tableView.deleteRows(at: [indexPath], with: .fade)
                tableView.endUpdates()
                
                print("delete data success, id = \(id!)")
            }
        }
    }
    
    @objc func checkBtnAction(_ sender: UIButton) {
        let id = sender.tag - checkBoxTag
        if id != 0 {
            var index = -1
            for (i, record) in myRecords.enumerated() {
                if (record.id as! Int32) == id {
                    index = i
                    break
                }
            }
            
            if index != -1 {
                //set Core Data
                let result = coreDataConnect.update(entityName, predicate: "id = \(id)", attributeInfo: ["done":(checkBoxStatus ? "false" : "true")])
                if result {
                    //change checkbox image
                    sender.setImage(UIImage(named: (checkBoxStatus ? "checkbox" : "check")), for: .normal)
                    
                    myRecords.remove(at: index)
                    
                    myTableView.beginUpdates()
                    myTableView.deleteRows(at: [IndexPath(row: index, section: 0)], with: .fade)
                    myTableView.endUpdates()
                    
                    print("change checkbtn status success, index = \(index), id = \(id)")
                } else {
                    print("error")
                }
            }
        }
    }
}
