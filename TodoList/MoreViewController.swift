//
//  MoreViewController.swift
//  TodoList
//
//  Created by Vicki Yang on 2020/4/21.
//  Copyright © 2020 Vicki Yang. All rights reserved.
//

import UIKit

class MoreViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    let fullsize = UIScreen.main.bounds.size
    let myUserDafaults = UserDefaults.standard
    var myTableView :UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.title = "關於"
        self.view.backgroundColor = #colorLiteral(red: 1.0, green: 1.0, blue: 1.0, alpha: 1.0)
        self.navigationController?.navigationBar.isTranslucent = false
        self.automaticallyAdjustsScrollViewInsets = false
        
        //UITableView
        self.myTableView = UITableView(frame: CGRect(x: 0, y: 0, width: fullsize.width, height: fullsize.height - 60), style: .grouped)
        self.myTableView.register(UITableViewCell.self, forCellReuseIdentifier: "Cell")
        self.myTableView.delegate = self
        self.myTableView.dataSource = self
        myTableView.allowsSelection = true
        self.view.addSubview(self.myTableView)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "Cell", for: indexPath) as UITableViewCell
        cell.accessoryType = .none
        
        if indexPath.section == 0 {
            cell.accessoryType = .disclosureIndicator
            cell.textLabel?.text = "已完成事項"
        } else if indexPath.section == 1 {
            let linkBtn = UIButton(frame: CGRect(x: 15, y: 0, width: fullsize.width, height: 40))
            linkBtn.setTitle("參考資訊", for: .normal)
            linkBtn.setTitleColor(#colorLiteral(red: 0, green: 0, blue: 0, alpha: 1), for: .normal)
            linkBtn.contentHorizontalAlignment = .left
            linkBtn.addTarget(self, action: #selector(goLinkUrl), for: .touchUpInside)
            cell.contentView.addSubview(linkBtn)
        }
        
        return cell
    }
    
    @objc func goLinkUrl() {
        let linkUrl = URL(string: "https://itisjoe.gitbooks.io/swiftgo/content/")
        UIApplication.shared.open(linkUrl!)
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        if indexPath.section == 0 {
            self.navigationController?.pushViewController(CheckedRecordsViewController(), animated: true)
        }
    }
    
    //有幾組 section
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        var title = ""
        switch section {
            case 0:
                title = "更多"
            case 1:
                title = "支援"
            default:
                title = ""
        }
        return title
    }
}
